package ma.octo.assignement.serviceImpl;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImpl implements AuditService {

    private Logger LOGGER = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Autowired
    private AuditRepository auditRepository;

    @Override
        public void audit(String message, EventType type) {

        LOGGER.info("Audit de l'événement {}",type.getType());
        Audit audit=new Audit();
        audit.setEventType(type);
        audit.setMessage(message);
        auditRepository.save(audit);
    }

}
