package ma.octo.assignement.serviceImpl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class VirementServiceImpl implements VirementService {
    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditServiceImpl auditService;
    @Autowired
    private VirementRepository virementRepository;

    @Override
    public List<Virement> loadAllVirements() {
        List<Virement> all = virementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Override
    public void createTransaction(VirementDto virementDto) throws CompteNonExistantException, TransactionException {


        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur_nom_prenom());
        Compte compteBeneficiaire = compteRepository
                .findByNrCompte(virementDto.getNrCompteBeneficiaire_rib());
        BigDecimal montantVirement=virementDto.getMontantOperation();

        if (compteEmetteur == null || compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (montantVirement==null || montantVirement.doubleValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montantVirement.intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (montantVirement.intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().trim().length()==0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().subtract(montantVirement).doubleValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }


        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montantVirement));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire
                .setSolde(new BigDecimal(compteBeneficiaire.getSolde().doubleValue() + montantVirement.doubleValue()));
        compteRepository.save(compteBeneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontant(montantVirement);
        virement.setMotifOperation(virementDto.getMotif());
        virement.setDateExecution(new Date());
        virementRepository.save(virement);

        auditService.audit("Virement depuis " + virementDto.getNrCompteEmetteur_nom_prenom() + " vers " + virementDto
                .getNrCompteBeneficiaire_rib() + " d'un montant de " + montantVirement
                , EventType.VIREMENT);
    }
    }



