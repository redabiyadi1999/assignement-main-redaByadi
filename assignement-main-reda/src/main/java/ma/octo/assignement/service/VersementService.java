package ma.octo.assignement.service;


import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import java.util.List;

public interface VersementService {
    List<Versement> loadAllVersements();
    void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException;

}
