package ma.octo.assignement.domain;


import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "Operation")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_OPERATION",discriminatorType = DiscriminatorType.STRING)
@Setter
@Getter
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal montant;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;
    @ManyToOne
    private Compte compteBeneficiaire;

    @Column(length = 200)
    private String motifOperation;
}
