package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class VirementDto {
  protected String nrCompteEmetteur_nom_prenom;
  protected String nrCompteBeneficiaire_rib;
  protected String motif;
  protected BigDecimal montantOperation;
  protected Date date;

}
