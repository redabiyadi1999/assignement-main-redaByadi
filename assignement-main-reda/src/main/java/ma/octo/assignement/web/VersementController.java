package ma.octo.assignement.web;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.VersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "Versement")
public class VersementController {

    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private VersementService versementService;
    @Autowired
    private VersementRepository versementRepository;

    @GetMapping("lister_versements")
    public List<Versement> loadAll() {
   return  versementService.loadAllVersements();
    }

    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto) throws TransactionException, CompteNonExistantException {
        versementService.createTransaction(versementDto);
    }

}
